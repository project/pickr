/**
 * @file
 * Init Pickr.
 */

(function ($, Drupal) {
  /**
   * Replaces the "Home" link with "Back to site" link.
   *
   * Back to site link points to the last non-administrative page the user
   * visited within the same browser tab.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the replacement functionality to the toolbar-escape-admin element.
   */
  Drupal.behaviors.pickr = {
    attach(context) {
      $(".color-picker")
        .once("pickr")
        .each(function (el) {
          $this = $(this);

          id = $this.attr("data-id");
          $id = $("[data-pickr=" + id + "]");

          var pickr = Pickr.create({
            el: this,
            theme: "classic",
            default: $id.val(),
            swatches: [
              "#0000FF",
              "#FF0000",
              "#00FF00",
              "#FFA500",
              "#A52A2A",
              "#800080",
              "#FFFF00",
            ],
            components: {
              preview: true,
              hue: true,
              interaction: {
                hex: true,
                input: true,
                clear: true,
                save: true,
              },
            },
          });

          pickr.on("save", function (color, instance) {
            var value = "";

            if (color) value = color.toHEXA().toString();

            $id.val(value);
          });
        });
    },
  };
})(jQuery, Drupal);
