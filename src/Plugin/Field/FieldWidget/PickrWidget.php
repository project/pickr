<?php

namespace Drupal\pickr\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'pickr' field widget.
 *
 * @FieldWidget(
 *   id = "pickr_widget",
 *   label = @Translation("Color Picker"),
 *   field_types = {
 *     "pickr"
 *   }
 * )
 */
class PickrWidget extends WidgetBase {


  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $pickrId = Html::getUniqueId('pickr-' . $this->fieldDefinition->getName());
    $value = isset($items[$delta]->value) ? $items[$delta]->value : '';

    $element += [
      '#type' => 'textfield',
      '#default_value' => $value,
      '#attributes' => [
        'data-pickr' => $pickrId,
      ],
      '#attached' => [
        'library' => [
          'pickr/pickr',
        ]
      ]
    ];

    $element['#suffix'] = "<div class=\"color-picker\" data-id=\"" . $pickrId . "\"></div>";

    return ['value' => $element];
  }
}
